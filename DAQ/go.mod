module example.com/DAQ

go 1.16

replace example.com/wfio => ../../goWFAnalysis/wfio

replace gonum.org/v1/gonum => gonum.org/v1/gonum v0.8.1-0.20210203072158-6eb8ed55bbee

replace github.com/go-latex/latex => github.com/go-latex/latex v0.0.0-20201211121425-9504e023060c

require (
	example.com/wfio v0.0.0-00010101000000-000000000000
	github.com/frankban/quicktest v1.13.0 // indirect
	go-hep.org/x/hep v0.28.6
)
