package DAQ

import (
	"encoding/gob"
	"fmt"
	"log"
	"math/rand"
	"net"
	"net/http"
	"sync"
	"time"

	"example.com/wfio"
	"go-hep.org/x/hep/groot"

	"github.com/wcharczuk/go-chart/v2"
)

var lock sync.Mutex

type Waveform struct {
	X, Y []float64
	BCID int
}

type DCS struct {
	T1, T2 float64
	VDAC   int
	Tmeas  time.Time
}

func (d DCS) String() string {
	return fmt.Sprintf("T1 = %v C, T2 = %v C, VDAC = %v V, measured at %v", d.T1, d.T2, d.VDAC, d.Tmeas.Format(time.RFC3339))
}

func GenerateData(data chan Waveform, stop chan bool) {

	const n int = 128
	bcid := 0
	nwf := 0

	start := time.Now()
	tick := time.NewTicker(500 * time.Millisecond)
	for {
		select {
		case <-tick.C:
			since := time.Since(start)
			rate := float64(nwf) / float64(since.Seconds())
			log.Printf("Acquiring data at %v Hz", rate)

		case <-stop:
			return
		default:
			t := make([]float64, n)
			A := make([]float64, n)

			for i := 0; i < n; i++ {
				t[i] = float64(i)
				A[i] = rand.NormFloat64()
			}

			wf := Waveform{X: t, Y: A, BCID: bcid}
			bcid++

			data <- wf
			nwf++
			time.Sleep(1 * time.Microsecond)
		}
	}

}

func GenerateDCS(data chan DCS, stop chan bool) {

	for {
		select {
		case <-stop:
			return
		default:

			dcs := DCS{T1: rand.Float64(), T2: rand.Float64(), VDAC: rand.Intn(100), Tmeas: time.Now()}
			data <- dcs

			time.Sleep(250 * time.Millisecond)
		}
	}

}

func AverageWF(wfs chan Waveform, Navg int, output chan Waveform) {

	outputy := make([]float64, 128)

	current := Waveform{}

	for i := 0; i < Navg; i++ {
		current = <-wfs
		for j, v := range current.Y {
			outputy[j] += v
		}
	}

	outputwf := Waveform{X: current.X, Y: outputy, BCID: current.BCID}

	output <- outputwf
}

func WaveformAverager(input chan Waveform, output chan Waveform, Navg int, stop chan bool) {

	n := 0
	buf := make(chan Waveform, 5*Navg)

	navgd := 0
	tick := time.NewTicker(500 * time.Millisecond)
	start := time.Now()
	for {
		select {
		case <-tick.C:
			since := time.Since(start)
			rate := float64(navgd) / float64(since.Seconds())
			log.Printf("Averaging at %v Hz", rate)
		case <-stop:
			return
		case inputwf := <-input:
			buf <- inputwf
			n++
			if n == Navg {
				n = 0
				go AverageWF(buf, Navg, output)
				navgd++
			}
		}
	}
}

func WaveforWriter(wf chan Waveform, filename string, stop chan bool) {

	f, err := groot.Create("test.root")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	nwr := 0

	rwr := wfio.ROOTWriter{File: f}
	tick := time.NewTicker(500 * time.Millisecond)

	start := time.Now()

	for {
		select {
		case <-tick.C:
			since := time.Since(start)
			rate := float64(nwr) / float64(since.Seconds())
			if len(wf) != 0 {
				log.Printf("Writing to disk at %v Hz", rate)
			} else {
				log.Printf("Buffer is empty, gimme waveforms")
			}
		case <-stop:
			return
		case inputwf := <-wf:
			Graph := wfio.Graph{X: inputwf.X, Y: inputwf.Y, Xlabel: "time (s)", Ylabel: "Amplitude (V)", Name: fmt.Sprintf("BCID%v", inputwf.BCID)}
			rwr.Write(Graph)
			nwr++
		}
	}
}

func WaveformTCPSender(wf chan Waveform, serverpath string, stop chan bool) {

	nwr := 0

	conn, err := net.Dial("tcp", serverpath)

	if err != nil {
		log.Fatal("Connection error :", err)
	}
	defer conn.Close()

	encoder := gob.NewEncoder(conn)

	start := time.Now()
	tick := time.NewTicker(500 * time.Millisecond)

	for {
		select {
		case <-tick.C:
			since := time.Since(start)
			rate := float64(nwr) / float64(since.Seconds())
			log.Printf("Writing to client at %v Hz", rate)
		case <-stop:
			return
		case inputwf := <-wf:
			encoder.Encode(inputwf)
			nwr++
		}
	}

}

func WaveformTCPReceiver(output chan Waveform, port int) {

	ln, err := net.Listen("tcp", fmt.Sprintf("192.168.1.101:%v", port))
	if err != nil {
		// handle error
		log.Fatal(err)
	}

	conn, err := ln.Accept() // this blocks until connection or error
	if err != nil {
		// handle error
		return
	}

	dec := gob.NewDecoder(conn)

	nwr := 0

	start := time.Now()

	wf := Waveform{}

	for {
		err = dec.Decode(&wf)
		if err != nil {
			log.Println("Connection was dropped!")
			return
		}
		output <- wf
		nwr++

		if nwr%10 == 0 {
			log.Printf("Receiving waveform at %v Hz", float64(nwr)/float64(time.Since(start).Seconds()))
		}
	}

}

func DCSDisplayer(dcs chan DCS, stop chan bool) {
	// http.HandleFunc("/", httpserver)
	// http.ListenAndServe(":8081", nil)

	ts := &chart.TimeSeries{
		XValues: []time.Time{},
		YValues: []float64{},
	}
	graph := &chart.Chart{
		Series: []chart.Series{ts},
	}

	h := func(w http.ResponseWriter, _ *http.Request) {

		w.Write([]byte("<meta http-equiv=\"refresh\" content=\"3\" />"))
		lock.Lock()
		if err := graph.Render(chart.PNG, w); err != nil {
			log.Printf("%v", err)
		}
		lock.Unlock()
	}

	http.HandleFunc("/", h)
	go http.ListenAndServe(":8081", nil)

	for {
		select {
		case <-stop:
			return
		case current := <-dcs:
			ts.XValues = append(ts.XValues, current.Tmeas)
			ts.YValues = append(ts.YValues, current.T1)
		}
	}

}
