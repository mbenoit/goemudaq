package main

import (
	"log"
	"time"

	"example.com/DAQ"
)

func main() {

	stop := make(chan bool, 5)

	RawWF_channel := make(chan DAQ.Waveform, 100)
	AvgWF_channel := make(chan DAQ.Waveform, 100)
	DCS_channel := make(chan DAQ.DCS, 100)

	go DAQ.GenerateData(RawWF_channel, stop)
	go DAQ.GenerateDCS(DCS_channel, stop)
	go DAQ.WaveformAverager(RawWF_channel, AvgWF_channel, 100, stop)
	go DAQ.WaveforWriter(AvgWF_channel, "data.root", stop)
	go DAQ.DCSDisplayer(DCS_channel, stop)

	time.Sleep(100 * time.Second)
	//stop <- true

	log.Println("Done!")

}
