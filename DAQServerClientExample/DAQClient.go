package main

import (
	"flag"
	"log"
	"time"

	"example.com/DAQ"
)

func main() {

	fileflag := flag.String("i", "localhost:8080", "ip:port to connect to")
	flag.Parse()

	stop := make(chan bool, 5)

	RawWF_channel := make(chan DAQ.Waveform, 100)
	AvgWF_channel := make(chan DAQ.Waveform, 100)
	DCS_channel := make(chan DAQ.DCS, 100)

	go DAQ.GenerateData(RawWF_channel, stop)
	go DAQ.GenerateDCS(DCS_channel, stop)
	go DAQ.WaveformAverager(RawWF_channel, AvgWF_channel, 100, stop)
	go DAQ.WaveformTCPSender(AvgWF_channel, *fileflag, stop)
	//	go DAQ.WaveforWriter(AvgWF_channel, "data.root", stop)
	go DAQ.DCSDisplayer(DCS_channel, stop)

	time.Sleep(100 * time.Second)
	//stop <- true

	log.Println("Done!")
}
