package main

import (
	"flag"
	"log"
	"time"

	"example.com/DAQ"
)

func main() {

	portflag := flag.Int("p", 8080, "port to listen to for data")
	flag.Parse()

	TCPWF_channel := make(chan DAQ.Waveform, 100)
	var stop chan bool
	go DAQ.WaveformTCPReceiver(TCPWF_channel, *portflag)
	go DAQ.WaveforWriter(TCPWF_channel, "data.root", stop)

	time.Sleep(100 * time.Second)

	stop <- true

	log.Println("Done!")
}
