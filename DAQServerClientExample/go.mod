module example.com/DAQClientServerExample

go 1.16

replace example.com/DAQ => ../DAQ

replace example.com/wfio => ../../gowfanalysis/wfio

replace gonum.org/v1/gonum => gonum.org/v1/gonum v0.8.1-0.20210203072158-6eb8ed55bbee

replace github.com/go-latex/latex => github.com/go-latex/latex v0.0.0-20201211121425-9504e023060c

require (
	example.com/DAQ v0.0.0-00010101000000-000000000000
	github.com/wcharczuk/go-chart/v2 v2.1.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
